let myApp= angular.module('myApp', []);

function operateOnScope($scope){
    function init(){
        $scope.number=0;
    }
    init();

    $scope.increase= function(){
        console.log("Increase called");
        $scope.number += 1;
    }
    $scope.decrease= function(){
        console.log("Decrease called");
        $scope.number -= 1;
    }
    $scope.reset= function(){
        console.log("Reset called");
        $scope.number=0;
    }
}

myApp.controller('numController', operateOnScope);

